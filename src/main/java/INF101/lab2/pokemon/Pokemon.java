package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {
    public String name;
    public int healthPoints;
    public int maxHealthPoints;
    public int strength;
    public Random random;

    public Pokemon(String name){
        this.name = name; 
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));

    }
    



    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        return healthPoints >0; 
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        System.out.println(this.getName() + " attacks " + target.getName() + "."); 
        target.damage(damageInflicted);
        if (!target.isAlive()){
            System.out.println(target.getName()+ " is defeated by " + this.getName());
        }
    }

    @Override
    public void damage(int damageTaken) {
        if (damageTaken >0){
          if (damageTaken >healthPoints){
            healthPoints=0; 
          }  
          else{
            healthPoints-=damageTaken;
          }
        }
        System.out.println(name + " takes " + damageTaken + " damage and is left with " + healthPoints + "/" + maxHealthPoints + " HP");
    }

    @Override
    public String toString() {
        String pokemonString = name+ " HP: (" + healthPoints+ "/" + maxHealthPoints +") STR: " + strength; 
        return pokemonString;
    }

}
